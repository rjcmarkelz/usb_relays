#Work in progress script for relay

#Importing all the necessary libraries
import sys
import serial
import time

#Command-line arguments: 1st argument is the minute at every hour the relay will turn on, the 2nd is the relay # that will turn on
#I made individual lists for each of the relays: this way I might be able to change individual elements later on
#Might be able to make this more efficient by including all of these lists into a dictionary
#Then have a for loop that goes through the dictionary (a relay number with an associated minute with it) and tries to turn on the relay like that
#This probably won't work because I'm resetting the lists every single time I run the script, I need to find a way for the script to run differently when I'm running the script a second time

relaynum1 = []
relaymin1 = []
if not relaynum1:
    relaynum1 = int(raw_input("What relay do you want to turn on?"))
    relaymin1 = int(raw_input("What minute do you want to turn the relay on?"))
    
relaynum2 = []
relaymin2 = []
if not relaynum2:
    relaynum1 = int(raw_input("What relay do you want to turn on?"))
    relaymin1 = int(raw_input("What minute do you want to turn the relay on?"))

relaynum3 = []
relaymin3 = []
if not relaynum3:
    relaynum1 = int(raw_input("What relay do you want to turn on?"))
    relaymin1 = int(raw_input("What minute do you want to turn the relay on?"))

relaynum4 = []
relaymin4 = []
if not relaynum4:
    relaynum1 = int(raw_input("What relay do you want to turn on?"))
    relaymin1 = int(raw_input("What minute do you want to turn the relay on?"))

relaynum5 = []
relaymin5 = []
if not relaynum5:
    relaynum1 = int(raw_input("What relay do you want to turn on?"))
    relaymin1 = int(raw_input("What minute do you want to turn the relay on?"))

relaynum6 = []
relaymin6 = []
if not relaynum6:
    relaynum1 = int(raw_input("What relay do you want to turn on?"))
    relaymin1 = int(raw_input("What minute do you want to turn the relay on?"))

relaynum7 = []
relaymin7 = []
if not relaynum7:
    relaynum1 = int(raw_input("What relay do you want to turn on?"))
    relaymin1 = int(raw_input("What minute do you want to turn the relay on?"))

relaynum8 = []
relaymin8 = []
if not relaynum8:
    relaynum1 = int(raw_input("What relay do you want to turn on?"))
    relaymin1 = int(raw_input("What minute do you want to turn the relay on?"))

relaynum9 = []
relaymin9 = []
if not relaynum9:
    relaynum9 = int(raw_input("What relay do you want to turn on?"))
    relaymin9 = int(raw_input("What minute do you want to turn the relay on?"))

relaynum10 = []
relaymin10 = []
if not relaynum1:
    relaynum10 = int(raw_input("What relay do you want to turn on?"))
    relaymin10 = int(raw_input("What minute do you want to turn the relay on?"))

relaynum11 = []
relaymin11 = []
if not relaynum11:
    relaynum11 = int(raw_input("What relay do you want to turn on?"))
    relaymin11 = int(raw_input("What minute do you want to turn the relay on?"))

relaynum12 = []
relaymin12 = []
if not relaynum12:
    relaynum12 = int(raw_input("What relay do you want to turn on?"))
    relaymin12 = int(raw_input("What minute do you want to turn the relay on?"))

#Defining the port the relay is connected to, and then designating a variable to access that port
portName = "COM7"
serPort = serial.Serial(portName, 19200, timeout = 1)

#Specfying the amount of time you want the relay to be turned on
ontime = 5
offtime = 0.1

#Script to check the relay number argument, and then convert it to hexadecimal to allow the relay to read our input
if time.gmtime(time.time())[4] == relaymin:
    while time.gmtime(time.time())[4] < relaymin + 5 and time.gmtime(time.time())[4] >= relaymin:
        if relaynum < 10:
            relayIndex = str(relaynum)h
        else:
            relayIndex = chr(55 + int(relaynum))
        serPort.write("relay on " + relayIndex +"\n\r")
    else:
        if relaynum < 10:
            relayIndex = str(relaynum)
        else:
            relayIndex = chr(55 + int(relaynum))
        serPort.write("relay off " + relayIndex +"\n\r")
elif time.gmtime(time.time())[4] != relaymin:
    x = abs(time.gmtime(time.time())[4] - relaymin) * 60
    time.sleep(x)
    if relaynum < 10:
            relayIndex = str(relaynum)
        else:
            relayIndex = chr(55 + int(relaynum))
        serPort.write("relay on " + relayIndex +"\n\r")
        
    
    
"""if relaynum < 10:
    relayIndex = str(relaynum)
else:
    relayIndex = chr(55 + int(relaynum))
serPort.write("relay on " + relayIndex +"\n\r")
print "The relay you turned on is " + str(relaynum)
time.sleep(ontime)
serPort.write("relay off " + relayIndex +"\n\r")
print "Relay " + relayIndex + " is now off."
time.sleep(offtime)"""

#Script in progress to get the relay to turn on hourly
'''while True:
    if time.gmtime(time.time())[4] < relaymin + 5:
        if relaynum < 10:
            relayIndex = str(relaynum)
        else:
            relayIndex = chr(55 + int(relaynum))
        serPort.write("relay on " + relayIndex +"\n\r")
        time.sleep(300)
    elif time.gmtime(time.time())[4] == relaymin + 5:
        serPort.write("relay off " + relayIndex +"\n\r")'''
        

