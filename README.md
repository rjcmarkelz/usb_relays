# README USB Relays

This repository is for scripts related to controlling Numato Labs [USB 16 relay module](http://numato.com/16-channel-usb-relay-module).  The goal is to develop a script where the user can specify a time interval for each relay to open and shut.

## Finding the device number
To be able to interacti with the USB card we need to know how the computer sees it.  On a mac (should be similar on linux) the device will be listed in /dev.  So go to terminal and type:

    ls /dev/tty.*
    
This may show you more than one device.  The relay module comes up on my computer as `/dev/tty.usbmodem1421`.

You can test this by typing `ls /dev/tty.*` alternately with the device plugged in and not plugged in.

I think that it will always be `tty.usbmodem.xxxx`.    I am not sure if it will always be 1421.

On a windows machine (and possibly on macs also) the device might be listed as a COM device but I have not investigated that yet.

## Controlling the relay board from the terminal.

We can use screen to open up a terminal to the USB board.  From the terminal type:

    screen /dev/tty.ysbmodem1421
    
Once that is done then the commands listed in the [Numato Labs user manual](http://numato.com/productdoc/usbrelay16/16ChannelUSBRelayModuleV7.pdf) can be used.  e.g.

    relay on 8
    relay off 8
    relay read 8
    
## Controlling the relay board from python.

**NOTE THIS IS ONLY WORKING FOR PYTHON 2.7, NOT PYTHON 3**

Before getting started, install the [pyserial](http://pyserial.sourceforge.net/) module.  From the terminal:

    easy_install-2.7 pyserial
    
OR

    pip2.7 install pyserial

(depending on your setup you may need to precede those commands with `sudo`


The Numato [example code](http://svn.numato.cc/dl.php?repname=numatocc&path=%2Fsamplecode%2Fpython%2Fusbrelay1_2_4_8%2F&isdir=1&rev=59&peg=59) is very helpful to get started.

I have modified the Numato code to demonstrate a simple loop.  See `RelayDemo.py` or `RelayDemo.ipynb` in the source section.  It is pretty much written to be played with interactively, so you may want to install [iPython](http://ipython.org/) and [iPython Notebook](http://ipython.org/notebook.html)

    