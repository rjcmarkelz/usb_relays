#! /usr/env/python2.7

#Basic script to demonstrate use of the Numato Labs USB 16 relay module
#Juiln Maloof
#August, 2014

#best run interactively using iPython or iPython notebook

import sys
import serial
import time


portName = "/dev/tty.usbmodem1421"
relayNum = 8 #which relay to play with

serPort = serial.Serial(portName, 19200, timeout=1) #initiate the serial port through python


#The relay board adresses the relays in hexadecimel.  
#This makes the appropriate adjustment
if (int(relayNum) < 10):
    relayIndex = str(relayNum)
else:
    relayIndex = chr(55 + int(relayNum))

#take a look
print(relayIndex)

#read the status of the relay
serPort.write("relay read "+ relayIndex + "\n\r")
response = serPort.read(25) #25 is the maximum number of bytes to read
print(response) # see the raw response

#format the resopnse
if(response.find("on") > 0):
	print "Relay " + str(relayNum) +" is ON"
elif(response.find("off") > 0):
	print "Relay " + str(relayNum) +" is OFF"

#toggle the relay on and off
serPort.write("relay on " + relayIndex +"\n\r")

serPort.write("relay off " + relayIndex +"\n\r")

#run a loop across each relay for X times
ontime = 1
offtime = .1
times = 1000
for i in range(times):
    for relay in range(16):
        if (int(relay) < 10):
            relayIndex = str(relay)
        else:
            relayIndex = chr(55 + int(relay))
        serPort.write("relay on " + relayIndex +"\n\r")
        time.sleep(ontime)
        serPort.write("relay off " + relayIndex +"\n\r")
        time.sleep(offtime)

#Turn off all relays
serPort.write("reset\n\r")

#Close the port
serPort.close()

